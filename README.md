# Cinta Coding Application

An Application that connects users with the others through commenting social platform.

## Environment Variables

We are using a public API (https://jsonplaceholder.typicode.com/)

| Key  | Example Value | Description               |
| ---- | ------------- | ------------------------- |
| none | development   | We are using a public API |

## Branching

Cinta Coding's multiple environments for future uses:

| Branch Name | Env         | Description                                          |
| ----------- | ----------- | ---------------------------------------------------- |
| development | development | A branch that used for engineer develop new features |
| staging     | staging     | A branch that used for software testers and hotfix   |
| main        | production  | A branch that used for our user                      |

- Staging used for only separating feature between development not the data. Development and staging use the same database.

## URLs

Cinta Coding is not deployed and we are using a public API (https://jsonplaceholder.typicode.com/)

| Env         | Web URL | API URL |
| ----------- | ------- | ------- |
| development | none    | none    |
| staging     | none    | none    |
| production  | none    | none    |

## How to Run?

- `pnpm instal` for initiating installations modules
- `pnpm dev` for running a server in development mode

## Deployment

| Environment | Description                          |
| ----------- | ------------------------------------ |
| development | based on merge in development branch |
| staging     | based on merge in staging branch     |
| production  | based on release tag                 |

## Development Rules

### Generals

- The import local component/library should be with alias path (use `@ReduxModules/Counter` instead of `../src/Redux/Modules/Counter`), here is available alias path:
  | Alias Path | Description |
  | --------------------- | ----------------------------------------------------------------------- |
  | @Atoms | To access atoms directory which inside component |
  | @Molecules | To access molecules directory inside component |
  | @Organisms | To access organisms directory inside component |
  | @Definitions | To access definitions directory inside src |
  | @Interfaces | To access interfaces directory inside src |
  | @Pages | To access pages directory |
  | @Redux | To access redux directory |
  | @ReduxModules | To access redux/modules directory |
  | @Static | To access public/static directory |

- The imported library should be pyramid sorted and also grouped by:
  | Order Number | Import Area Description |
  | --------------------- | ----------------------------------------------------------------------- |
  | 1 | Import external library here |
  | 2 | Import local component here |
  | 3 | Import redux family here |
  | 4 | Import util/helper/etc function here |
  | 5 | Import Interface here |

### Components

- Component should be written in functional component
- A function in functional component should be wrapped by `useCallback` to prevent a useless instantiation
- A function which has expensive calculation should be wrapped by `useMemo` to prevent a useless calculation
- Functional component should be exported with `React.memo` to prevent a useless rendering
- Interface should exist in a functional component to register incoming props.
- Every component should have snapshot testing
- Every new component shoud have property data-testid with prefix typescript validation, example: `data-testid=button-login`
