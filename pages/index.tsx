import React from "react";
import Head from "next/head";
import { Box } from "@mui/material";
import type { NextPage } from "next";
import { AppBarLayout } from "../src";

const Home: NextPage = () => {
  return (
    <>
      <Head>
        <title>One Code</title>
        <meta name="viewport" content="initial-scale=1, width=device-width" />
      </Head>
      <AppBarLayout>
        <Box justifyContent="center" display="flex">
          <img
            width="100%"
            src="/static/illustrations/Tiny HR specialist looking for recruits for job.jpg"
            alt="Error 404"
          />
        </Box>
      </AppBarLayout>
    </>
  );
};

export default Home;
