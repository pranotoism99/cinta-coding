import React, { useEffect, useState } from "react";
import axios from "axios";
import { useRouter } from "next/router";
import { AppBarLayout } from "../../src";
import { Box, Button, TextField, Container, Typography } from "@mui/material";

function LoginPage() {
  const router = useRouter();

  const [users, setUsers] = useState<any>([]);
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [isError, setIsError] = useState(false);

  useEffect(() => {
    axios
      .get("https://jsonplaceholder.typicode.com/users")
      .then(function (response) {
        setUsers(response.data);
      })
      .catch(function (error) {
        if (error.response) {
          console.log(error.response.data);
          console.log(error.response.status);
          console.log(error.response.headers);
        } else if (error.request) {
          console.log(error.request);
        } else {
          console.log("Error", error.message);
        }
        console.log(error.config);
      });
  }, []);

  const handleLogin = () => {
    let isLogin = users.find(
      (user: { username: string }) =>
        user.username === username && user.username === password
    );
    if (isLogin) {
      localStorage.setItem("user data", JSON.stringify(isLogin));
      router.push(`/dashboard`);
    } else {
      setIsError(true);
    }
  };

  return (
    <>
      <AppBarLayout location="login">
        <Container maxWidth={"xs"}>
          <Box
            height="calc(100vh - 48px)"
            display="flex"
            flexDirection="column"
            alignItems="center"
            justifyContent="center"
          >
            <Box maxWidth={280}>
              <Typography
                variant="h6"
                fontWeight={900}
                component="div"
                textAlign="center"
              >
                Login Page
              </Typography>
              <TextField
                value={username}
                onChange={(e) => setUsername(e.target.value)}
                inputProps={{
                  style: {
                    textAlign: "center",
                  },
                }}
                sx={{
                  px: 5,
                  mt: 8,
                  border: "1px solid #1976d2",
                  borderRadius: 20,
                  ".MuiInputBase-root": {
                    py: 1,
                    ":before": {
                      borderBottom: "none",
                    },
                    ":after": {
                      borderBottom: "none",
                    },
                  },
                }}
                required
                placeholder="username"
                variant="standard"
                fullWidth
                type="email"
              />
              <TextField
                inputProps={{
                  style: {
                    textAlign: "center",
                  },
                }}
                sx={{
                  my: 4,
                  px: 5,
                  border: "1px solid #1976d2",
                  borderRadius: 20,
                  ".MuiInputBase-root": {
                    py: 1,
                    ":before": {
                      borderBottom: "none",
                    },
                    ":after": {
                      borderBottom: "none",
                    },
                  },
                }}
                placeholder="password"
                required
                variant="standard"
                fullWidth
                value={password}
                onChange={(e) => setPassword(e.target.value)}
              />
              <Button
                fullWidth
                variant="contained"
                sx={{ borderRadius: 20, p: 1.2 }}
                onClick={handleLogin}
              >
                Login
              </Button>
            </Box>
            {isError && (
              <Typography
                variant="caption"
                fontWeight={900}
                color="error"
                textAlign="center"
                sx={{ pt: 3 }}
              >
                Can not find user
              </Typography>
            )}
          </Box>
        </Container>
      </AppBarLayout>
    </>
  );
}

export default LoginPage;
