import React, { useEffect, useState } from "react";

import {
  Box,
  Grid,
  Container,
  Typography,
  CircularProgress,
} from "@mui/material";
import axios from "axios";
import { useRouter } from "next/router";
import { GetServerSideProps } from "next";
import { AppBarLayout } from "../../../src";
import KeyboardBackspaceIcon from "@mui/icons-material/KeyboardBackspace";
import ChatBubbleOutlineIcon from "@mui/icons-material/ChatBubbleOutline";

function DashboardPage() {
  const router = useRouter();

  const { id } = router.query as { id: string };

  const [post, setPost] = useState<any>();
  const [user, setUser] = useState<any>();
  const [comments, setComments] = useState([]);
  const [isCommentsOpen, setIsCommentsOpen] = useState(false);

  useEffect(() => {
    axios
      .get("https://jsonplaceholder.typicode.com/posts/" + id)
      .then(function (responseGetPost) {
        axios
          .get("https://jsonplaceholder.typicode.com/users")
          .then(function (responseGetUsers) {
            let users = responseGetUsers.data;
            let userName = users.find(
              (user: { id: number }) => user.id === responseGetPost.data.userId
            );
            setUser(userName);
            setPost(responseGetPost.data);
          });
      })
      .catch(function (error) {
        if (error.response) {
          console.log(error.response.data);
          console.log(error.response.status);
          console.log(error.response.headers);
        } else if (error.request) {
          console.log(error.request);
        } else {
          console.log("Error", error.message);
        }
        console.log(error.config);
      });

    axios
      .get("https://jsonplaceholder.typicode.com/posts/" + id + "/comments")
      .then(function (response) {
        setComments(response.data);
      });
  }, []);

  return (
    <>
      <AppBarLayout location="dashboard">
        <Container maxWidth={"sm"}>
          <KeyboardBackspaceIcon
            fontSize="large"
            onClick={() => router.push(`/dashboard`)}
            sx={{
              ":hover": {
                cursor: "pointer",
              },
            }}
          />
          <Box
            display="flex"
            alignItems="flex-start"
            justifyContent="flex-start"
            width="100%"
            mt={1.5}
            px={1}
          >
            {!post ? (
              <CircularProgress />
            ) : (
              <>
                <Typography
                  variant="h6"
                  fontWeight={900}
                  textAlign="left"
                  sx={{ pt: 4.5 }}
                >
                  {user?.username ?? <CircularProgress />}
                </Typography>
                <Box ml={4}>
                  <Typography variant="body1" textAlign="left" color="GrayText">
                    {post.title}
                  </Typography>
                  <Typography
                    variant="body1"
                    textAlign="left"
                    color="#bcbcbc"
                    sx={{ mt: 2 }}
                  >
                    {post.body}
                  </Typography>
                  {!isCommentsOpen ? (
                    <Box display="flex" alignItems="center" mt={2}>
                      <Box
                        onClick={() => setIsCommentsOpen(true)}
                        display="flex"
                        alignItems="center"
                        sx={{
                          ":hover": {
                            cursor: "pointer",
                          },
                        }}
                      >
                        <ChatBubbleOutlineIcon
                          fontSize="small"
                          sx={{ color: "#1976d2", mr: 1.5 }}
                        />
                        <Typography
                          variant="subtitle2"
                          fontWeight={900}
                          textAlign="left"
                          color="#1976d2"
                        >
                          {comments.length}
                        </Typography>
                      </Box>
                    </Box>
                  ) : (
                    <Box mt={2.5}>
                      <Typography
                        variant="body1"
                        textAlign="left"
                        fontWeight={900}
                        color="GrayText"
                      >
                        All Comment
                      </Typography>
                      {comments.map((comment: any, index) => {
                        return (
                          <Grid key={index} spacing={1} container mt={2}>
                            <Grid item xs={12} md={1}>
                              <Typography
                                variant="h6"
                                fontWeight={900}
                                textAlign="left"
                              >
                                {comment?.name.split(" ")[0] ?? ""}
                              </Typography>
                            </Grid>
                            <Grid item md={1.75}></Grid>
                            <Grid item xs={12} md={9.25}>
                              <Typography
                                variant="body1"
                                textAlign="left"
                                color="#bcbcbc"
                              >
                                {comment?.body ?? ""}
                              </Typography>
                            </Grid>
                          </Grid>
                        );
                      })}
                    </Box>
                  )}
                </Box>
              </>
            )}
          </Box>
        </Container>
      </AppBarLayout>
    </>
  );
}

export default DashboardPage;

export const getServerSideProps: GetServerSideProps = async () => ({
  props: {},
});
