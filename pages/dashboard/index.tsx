import React, { useEffect, useState } from "react";
import axios from "axios";
import { useRouter } from "next/router";
import { AppBarLayout } from "../../src";
import {
  Box,
  Button,
  TextField,
  Container,
  Typography,
  InputAdornment,
  CircularProgress,
} from "@mui/material";
import SearchIcon from "@mui/icons-material/Search";
import ChatBubbleOutlineIcon from "@mui/icons-material/ChatBubbleOutline";

function DashboardPage() {
  const router = useRouter();

  const [search, setSearch] = useState("");
  const [comment, setComment] = useState(0);
  const [users, setUsers] = useState<any>([]);
  const [posts, setPosts] = useState<any>([]);
  let [currentPage, setCurrentPage] = useState(1);

  useEffect(() => {
    axios
      .get("https://jsonplaceholder.typicode.com/users")
      .then(function (response) {
        setUsers(response.data);
      })
      .catch(function (error) {
        if (error.response) {
          console.log(error.response.data);
          console.log(error.response.status);
          console.log(error.response.headers);
        } else if (error.request) {
          console.log(error.request);
        } else {
          console.log("Error", error.message);
        }
        console.log(error.config);
      });

    axios
      .get("https://jsonplaceholder.typicode.com/posts")
      .then(function (response) {
        function chunkArray(arr: string | any[], size: number): any[] {
          return arr.length > size
            ? [arr.slice(0, size), ...chunkArray(arr.slice(size), size)]
            : [arr];
        }
        setPosts(chunkArray(response.data, 10));
      })
      .catch(function (error) {
        if (error.response) {
          console.log(error.response.data);
          console.log(error.response.status);
          console.log(error.response.headers);
        } else if (error.request) {
          console.log(error.request);
        } else {
          console.log("Error", error.message);
        }
        console.log(error.config);
      });
  }, []);

  const handlePrevPage = () => {
    setCurrentPage((prev) => prev - 1);
  };

  const handleNextPage = () => {
    setCurrentPage((prev) => prev + 1);
  };

  return (
    <>
      <AppBarLayout location="dashboard">
        <Container maxWidth={"sm"}>
          <Box
            display="flex"
            flexDirection="column"
            alignItems="center"
            justifyContent="center"
          >
            <TextField
              value={search}
              onChange={(e) => setSearch(e.target.value)}
              InputProps={{
                endAdornment: (
                  <InputAdornment position="start">
                    <SearchIcon fontSize="large" sx={{ color: "black" }} />
                  </InputAdornment>
                ),
              }}
              inputProps={{
                style: {
                  textAlign: "center",
                },
              }}
              sx={{
                backgroundColor: "#eff4f7",
                ".MuiInputBase-root": {
                  py: 1,
                  ":before": {
                    borderBottom: "none",
                  },
                  ":after": {
                    borderBottom: "none",
                  },
                },
                px: 2,
                border: "none",
                borderRadius: 20,
              }}
              required
              placeholder="Search"
              variant="standard"
              fullWidth
              type="email"
            />
            {posts.length === 0 && <CircularProgress />}
            {posts.map(
              (
                postsPage: { userId: number; title: string; id: number }[],
                index: number
              ) => (
                <div key={index}>
                  {index + 1 === currentPage && (
                    <>
                      {postsPage.map(
                        (
                          postsPage: {
                            userId: number;
                            title: string;
                            id: number;
                          },
                          index2: string | number | null | undefined
                        ) => {
                          let userPost = users.find(
                            (user: { id: number }) =>
                              user.id === postsPage.userId
                          );

                          axios
                            .get(
                              "https://jsonplaceholder.typicode.com/posts/" +
                                postsPage.id +
                                "/comments"
                            )
                            .then(function (response) {
                              setComment(response.data.length);
                            });

                          return (
                            <Box
                              key={index2}
                              display="flex"
                              alignItems="flex-start"
                              justifyContent="flex-start"
                              width="100%"
                              mt={3}
                            >
                              <Typography
                                variant="h6"
                                fontWeight={900}
                                textAlign="left"
                              >
                                {userPost?.username ?? <CircularProgress />}
                              </Typography>

                              <Box ml={4.5}>
                                <Typography
                                  variant="body1"
                                  textAlign="left"
                                  color="GrayText"
                                >
                                  {postsPage.title}
                                </Typography>
                                <Box display="flex" alignItems="center" mt={1}>
                                  <Box
                                    onClick={() =>
                                      router.push(`/posts/${postsPage.id}`)
                                    }
                                    display="flex"
                                    alignItems="center"
                                    sx={{
                                      ":hover": {
                                        cursor: "pointer",
                                      },
                                    }}
                                  >
                                    <ChatBubbleOutlineIcon
                                      fontSize="small"
                                      sx={{ color: "#1976d2", mr: 1.5 }}
                                    />
                                    <Typography
                                      variant="subtitle2"
                                      fontWeight={900}
                                      textAlign="left"
                                      color="#1976d2"
                                    >
                                      {comment}
                                    </Typography>
                                  </Box>
                                  <Typography
                                    onClick={() =>
                                      router.push(`/posts/${postsPage.id}`)
                                    }
                                    sx={{
                                      ":hover": {
                                        cursor: "pointer",
                                      },
                                    }}
                                    variant="subtitle2"
                                    fontWeight={900}
                                    textAlign="left"
                                    color="#1976d2"
                                    ml={4.5}
                                  >
                                    Detail
                                  </Typography>
                                </Box>
                              </Box>
                            </Box>
                          );
                        }
                      )}
                    </>
                  )}
                </div>
              )
            )}
          </Box>
          <Box
            display="flex"
            alignItems="center"
            justifyContent="flex-end"
            mr={12}
            mt={7}
            mb={3}
          >
            <Button
              variant="text"
              disabled={currentPage === 1}
              onClick={handlePrevPage}
              sx={{ textTransform: "lowercase" }}
            >
              <Typography
                sx={{
                  px: 2,
                }}
                variant="body1"
                textAlign="left"
                color="GrayText"
              >
                prev
              </Typography>
            </Button>
            {posts.map((_: any, index: number) => (
              <Box key={index}>
                <Typography
                  sx={{
                    px: 2,
                    borderBottom:
                      currentPage === index + 1 ? "2px solid #1976d2" : "none",
                    ":hover": {
                      cursor: "pointer",
                    },
                  }}
                  variant="body1"
                  textAlign="left"
                  color="GrayText"
                  onClick={() => setCurrentPage(index + 1)}
                >
                  {index + 1}
                </Typography>
              </Box>
            ))}
            <Button
              variant="text"
              disabled={currentPage === posts.length}
              onClick={handleNextPage}
              sx={{ textTransform: "lowercase" }}
            >
              <Typography
                sx={{
                  px: 2,
                }}
                variant="body1"
                textAlign="left"
                color="GrayText"
              >
                next
              </Typography>
            </Button>
          </Box>
        </Container>
      </AppBarLayout>
    </>
  );
}

export default DashboardPage;
