import React, { useEffect, useState } from "react";
import axios from "axios";
import { useRouter } from "next/router";
import { GetServerSideProps } from "next";
import { AppBarLayout } from "../../src";
import { Box, Grid, Container, Typography } from "@mui/material";
import KeyboardBackspaceIcon from "@mui/icons-material/KeyboardBackspace";

function UserPage() {
  const router = useRouter();

  const [user, setUser] = useState<any>();

  useEffect(() => {
    axios
      .get(
        "https://jsonplaceholder.typicode.com/users/" +
          JSON.parse(localStorage.getItem("user data") as any).id
      )
      .then(function (response) {
        setUser(response.data);
      })
      .catch(function (error) {
        if (error.response) {
          console.log(error.response.data);
          console.log(error.response.status);
          console.log(error.response.headers);
        } else if (error.request) {
          console.log(error.request);
        } else {
          console.log("Error", error.message);
        }
        console.log(error.config);
      });
  }, []);

  return (
    <>
      <AppBarLayout location="dashboard">
        <Container maxWidth={"sm"}>
          <KeyboardBackspaceIcon
            fontSize="large"
            onClick={() => router.push(`/dashboard`)}
            sx={{
              ":hover": {
                cursor: "pointer",
              },
            }}
          />
          <Container maxWidth={"xs"}>
            <Grid container spacing={2.25} mt={3} justifyContent="center">
              <Grid item xs={2.75}>
                <Typography
                  variant="subtitle1"
                  textAlign="left"
                  color="GrayText"
                  fontWeight={900}
                >
                  Username
                </Typography>
              </Grid>
              <Grid item xs={2.5}>
                <Typography
                  variant="subtitle1"
                  fontWeight={900}
                  textAlign="left"
                  color="GrayText"
                >
                  :
                </Typography>
              </Grid>
              <Grid item xs={5.75}>
                <Typography
                  variant="subtitle1"
                  fontWeight={900}
                  textAlign="left"
                >
                  {user?.name ?? ""}
                </Typography>
              </Grid>
              <Grid item xs={2.75}>
                <Typography
                  variant="subtitle1"
                  textAlign="left"
                  color="GrayText"
                  fontWeight={900}
                >
                  Email
                </Typography>
              </Grid>
              <Grid item xs={2.5}>
                <Typography
                  variant="subtitle1"
                  fontWeight={900}
                  textAlign="left"
                  color="GrayText"
                >
                  :
                </Typography>
              </Grid>
              <Grid item xs={5.75}>
                <Typography
                  variant="subtitle1"
                  fontWeight={900}
                  textAlign="left"
                >
                  {user?.email ?? ""}
                </Typography>
              </Grid>
              <Grid item xs={2.75}>
                <Typography
                  variant="subtitle1"
                  textAlign="left"
                  color="GrayText"
                  fontWeight={900}
                >
                  Address
                </Typography>
              </Grid>
              <Grid item xs={2.5}>
                <Typography
                  variant="subtitle1"
                  fontWeight={900}
                  textAlign="left"
                  color="GrayText"
                >
                  :
                </Typography>
              </Grid>
              <Grid item xs={5.75}>
                <Typography
                  variant="subtitle1"
                  fontWeight={900}
                  textAlign="left"
                >
                  {user?.name ?? ""}
                </Typography>
              </Grid>
              <Grid item xs={2.75}>
                <Typography
                  variant="subtitle1"
                  textAlign="left"
                  color="GrayText"
                  fontWeight={900}
                >
                  Phone
                </Typography>
              </Grid>
              <Grid item xs={2.5}>
                <Typography
                  variant="subtitle1"
                  fontWeight={900}
                  textAlign="left"
                  color="GrayText"
                >
                  :
                </Typography>
              </Grid>
              <Grid item xs={5.75}>
                <Typography
                  variant="subtitle1"
                  fontWeight={900}
                  textAlign="left"
                >
                  {user?.phone ?? ""}
                </Typography>
              </Grid>
            </Grid>
          </Container>
        </Container>
      </AppBarLayout>
    </>
  );
}

export default UserPage;

export const getServerSideProps: GetServerSideProps = async () => ({
  props: {},
});
