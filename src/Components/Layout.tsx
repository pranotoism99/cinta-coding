import * as React from "react";
import { useRouter } from "next/router";
import Toolbar from "@mui/material/Toolbar";
import { styled } from "@mui/material/styles";
import Typography from "@mui/material/Typography";
import CssBaseline from "@mui/material/CssBaseline";
import useScrollTrigger from "@mui/material/useScrollTrigger";

import Fab from "@mui/material/Fab";
import Box from "@mui/material/Box";
import Fade from "@mui/material/Fade";
import { Button } from "@mui/material";
import Container from "@mui/material/Container";
import KeyboardArrowUpIcon from "@mui/icons-material/KeyboardArrowUp";
import Tooltip, { TooltipProps, tooltipClasses } from "@mui/material/Tooltip";

const BootstrapTooltip = styled(({ className, ...props }: TooltipProps) => (
  <Tooltip
    {...props}
    arrow
    placement="bottom-end"
    classes={{ popper: className }}
  />
))(({ theme }) => ({
  [`& .${tooltipClasses.arrow}`]: {
    color: "#1976d2",
  },
  [`& .${tooltipClasses.tooltip}`]: {
    backgroundColor: "#fff",
    color: "black",
    borderRadius: 8,
    border: "1px solid #1976d2",
  },
}));

interface Props {
  /**
   * Injected by the documentation to work in an iframe.
   * You won't need it on your project.
   */
  window?: () => Window;
  location?: "login" | "dashboard";
  children: React.ReactElement;
}

function ScrollTop(props: Props) {
  const { children, window } = props;
  const trigger = useScrollTrigger({
    target: window ? window() : undefined,
    disableHysteresis: true,
    threshold: 100,
  });

  const handleClick = (event: React.MouseEvent<HTMLDivElement>) => {
    const anchor = (
      (event.target as HTMLDivElement).ownerDocument || document
    ).querySelector("#back-to-top-anchor");

    if (anchor) {
      anchor.scrollIntoView({
        block: "center",
      });
    }
  };

  return (
    <Fade in={trigger}>
      <Box
        onClick={handleClick}
        role="presentation"
        sx={{ position: "fixed", bottom: 16, right: 16 }}
      >
        {children}
      </Box>
    </Fade>
  );
}

export function AppBarLayout(props: Props) {
  const router = useRouter();

  const [username, setUsername] = React.useState("");

  React.useEffect(() => {
    setUsername(JSON.parse(localStorage.getItem("user data") as any).username);
  }, []);

  return (
    <Container maxWidth="xl">
      <CssBaseline />
      {props.location !== "login" && (
        <>
          <Container maxWidth={"lg"}>
            <Box
              display="flex"
              alignItems="center"
              justifyContent="space-between"
              mt={3.5}
            >
              <Typography variant="h4" fontWeight={900} component="div">
                Cinta Coding
              </Typography>
              {props.location === "dashboard" && (
                <Typography
                  variant="h5"
                  fontWeight={900}
                  component="div"
                  borderBottom="3px solid #1976d2"
                  padding="16px 24px 8px 24px"
                  mt={1}
                  color="gray"
                >
                  Post
                </Typography>
              )}
              {!props.location && (
                <Button
                  variant="contained"
                  sx={{ borderRadius: 20, px: 4 }}
                  onClick={() => router.push(`/login`)}
                >
                  Login
                </Button>
              )}
              {props.location === "dashboard" && (
                <Typography variant="h4" fontWeight={900} component="div">
                  Welcome,{" "}
                  <BootstrapTooltip
                    title={
                      <Box
                        display="flex"
                        alignItems="center"
                        justifyContent="center"
                      >
                        <Button
                          variant="text"
                          sx={{ textTransform: "capitalize" }}
                          onClick={() => router.push(`/user`)}
                        >
                          <Typography
                            variant="caption"
                            color="black"
                            fontWeight={900}
                            py={0.5}
                            px={2.5}
                          >
                            Detail Profile
                          </Typography>
                        </Button>
                      </Box>
                    }
                  >
                    <b
                      style={{
                        color: "#1976d2",
                        cursor: "pointer",
                      }}
                    >
                      {username}
                    </b>
                  </BootstrapTooltip>
                </Typography>
              )}
            </Box>
          </Container>
          <Toolbar id="back-to-top-anchor" />
        </>
      )}
      <Box sx={{ minHeight: "100vh", width: "100%" }}>
        <Container maxWidth={"md"}>
          <Box mb={10}>{props.children}</Box>
        </Container>
      </Box>
      <ScrollTop {...props}>
        <Fab size="small" aria-label="scroll back to top">
          <KeyboardArrowUpIcon />
        </Fab>
      </ScrollTop>
    </Container>
  );
}
